#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
/* This program uses the Relic library. To install the required curves, run the script install_relic.sh by first making it executable with the following command: chmod +x install_relic.sh . Then, execute it with: ./install_relic.sh .
 *This program compiles and runs the benchmark.c file with various curves, all at the 128-bit security level. For more accurate results, it's recommended to disable Turbo Boost before running the program. You can do this with the following command: $echo "1" | sudo tee /sys/devices/system/cpu/intel_pstate/no_turbo . To verify that Turbo Boost is disabled, run: $cat /sys/devices/system/cpu/intel_pstate/no_turbo . If you need to re-enable Turbo Boost, use: echo "0" | sudo tee /sys/devices/system/cpu/intel_pstate/no_turbo . To compile the program, use the command: gcc -Wall -o bench bench.c . You can then run the program with: ./bench */


int main() {

    // Output file name
    const char result_file[] = "benchmark_results.txt";


    // Open the result file for writing
    FILE *fp = fopen(result_file, "w");
    if (fp == NULL) {
        perror("Error opening file");
        return EXIT_FAILURE;
    }

   // Compilation for BN12-382
    printf("Compilation successful for BN12 382. Running program...\n");
    system("gcc -Wall -o benchmark benchmark.c  -I ~/relic/include -I ~/relic/target-382/include -L ~/relic/target-382/lib -lrelic_s_bn382 -lgmp >> benchmark_results.txt 2>&1");
    system("./benchmark >> benchmark_results.txt 2>&1");

     // Compilation for BLS12 377
    printf("Compilation successful for BLS12 377. Running program...\n");
    system("gcc -Wall -o benchmark benchmark.c  -I ~/relic/include -I ~/relic/target-377/include -L ~/relic/target-377/lib -lrelic_s_bls377 -lgmp >> benchmark_results.txt 2>&1");
    system("./benchmark >> benchmark_results.txt 2>&1");


   // Compilation for BLS12 381
    printf("Compilation successful for BLS12 381. Running program...\n");
    system("gcc -Wall -o benchmark benchmark.c  -I ~/relic/include -I ~/relic/target-381/include -L ~/relic/target-381/lib -lrelic_s_bls381 -lgmp >> benchmark_results.txt 2>&1");
    system("./benchmark >> benchmark_results.txt 2>&1");


           // Compilation for KSS16 330
    printf("Compilation successful for KSS16 330. Running program...\n");
    system("gcc -Wall -o benchmark benchmark.c -I ~/relic/include -I ~/relic/target-330/include -L ~/relic/target-330/lib -lrelic_s_kss330 -lgmp >> benchmark_results.txt 2>&1");
    system("./benchmark >> benchmark_results.txt 2>&1");



    // Compilation for KSS18 354
    printf("Compilation successful for KSS18 354. Running program...\n");
    system("gcc -Wall -o benchmark benchmark.c -I ~/relic/include -I ~/relic/target-354/include -L ~/relic/target-354/lib -lrelic_s_kss354 -lgmp >> benchmark_results.txt 2>&1");
    system("./benchmark >> benchmark_results.txt 2>&1");


           // Compilation for BLS24 315
    printf("Compilation successful for BLS24 315. Running program...\n");
    system("gcc -Wall -o benchmark benchmark.c -I ~/relic/include -I ~/relic/target-315/include -L ~/relic/target-315/lib -lrelic_s_bls315 -lgmp >> benchmark_results.txt 2>&1");
    system("./benchmark >> benchmark_results.txt 2>&1");

            // Compilation for BLS24 317
    printf("Compilation successful for BLS24 317. Running program...\n");
    system("gcc -Wall -o benchmark benchmark.c -I ~/relic/include -I ~/relic/target-317/include -L ~/relic/target-317/lib -lrelic_s_bls317 -lgmp >> benchmark_results.txt 2>&1");
    system("./benchmark >> benchmark_results.txt 2>&1");




    // Close the result file
    fclose(fp);

    return EXIT_SUCCESS;
}
