#include <stdio.h>
#include <relic.h>

void generate_random_bytes(uint8_t *bytes, size_t size) {
    for (size_t i = 0; i < size; i++) {
        bytes[i] = rand() % 256;  // Remplit le tableau avec des valeurs aléatoires entre 0 et 255
    }
}

int main() {

        // Declaration of the variables
        g1_t p, a,mapped_g1;
        g2_t q, b, mapped_g2;
        gt_t e, c;
        bn_t o, r;
        uint8_t bytes[32];

        // Initialisation of Relic, the paring friendly curve and the variables
        core_init();
        bench_init();
        pc_core_init();
        pc_param_set_any();
        g1_new(p);
        g1_new(a);
        g1_new(mapped_g1);
        g2_new(q);
        g2_new(b);
        g2_new(mapped_g2);
        gt_new(e);
        gt_new(c);
        bn_new(o);
        bn_new(r);
        pc_get_ord(o);
        int N=10000;

       /* int lambda;
        lambda=ep_param_level();
        printf("secu level = %d\n",lambda);*/


        // Print of some information about the curve
        pc_param_print();

        int size_g1 = g1_size_bin(p, 1);
        int size_g2 = g2_size_bin(q, 1);
        int size_gt = gt_size_bin(e, 1);

        // Print the elements' size according to the group they belong to
        printf("Size of element in G1: %d bytes\n", size_g1);
        printf("Size of element in G2: %d bytes\n", size_g2);
        printf("Size of element in GT: %d bytes\n", size_gt);
        printf("\n");

        // Random choice of the variables
        g1_rand(a);
        g2_rand(b);
        gt_rand(c);




     generate_random_bytes(bytes, sizeof(bytes));


        // Benchmark
       BENCH_FEW("Random generation over Fp", {
             bn_rand_mod(r, o);
        }, N);

        BENCH_FEW("Random generation over G1", {
            g1_rand(p);
        }, N);

            BENCH_FEW("Random generation over G2", {
            g2_rand(q);
        }, N);

            BENCH_FEW("Random generation over GT", {
            gt_rand(e);
        }, N);


        BENCH_FEW("Scalar multiplication on G1", {
            g1_mul(p, p, r);
        }, N);

        BENCH_FEW("Scalar multiplication on G2", {
            g2_mul(q, q, r);
        }, N);

        BENCH_FEW("Exponentiation in GT", {
            gt_exp(e, c, r);
        }, N);


        BENCH_FEW("Addition on G1", {
            g1_add(p, p, a);
        }, N);

        BENCH_FEW("Addition on G2", {
            g2_add(q, q, b);
        }, N);

        BENCH_FEW("Multiplication in GT", {
            gt_mul(e, c,e);
        }, N);


        BENCH_FEW("Negation in G1", {
            g1_neg(p, a);
        }, N);

        BENCH_FEW("Negation in G2", {
            g2_neg(q,b);
        }, N);


        BENCH_FEW("Inversion in GT", {
            gt_inv(e,c);
        }, N);

        BENCH_FEW("Pairing", {
            pc_map(e, p, q);
        }, N);

            BENCH_FEW("Mapping bytes to G1 element", {
            g1_map(mapped_g1, bytes, sizeof(bytes));
        }, N);

                  BENCH_FEW("Mapping bytes to G2 element", {
            g2_map(mapped_g2, bytes, sizeof(bytes));
        }, N);



        // Cleaning memory
        bn_free(o);
        bn_free(r);
        g1_free(p);
        g1_free(a);
        g1_free(mapped_g1);
        g2_free(q);
        g2_free(b);
        g2_free(mapped_g2);
        gt_free(e);
        gt_free(c);
        bench_clean();
        pc_core_clean();
        core_clean();

    return 0;
}
