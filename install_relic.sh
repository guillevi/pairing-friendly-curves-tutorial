#!/bin/bash

# Cloner le dépôt Relic
git clone https://github.com/relic-toolkit/relic.git
cd relic

# Fonction pour installer une courbe
install_curve() {
    curve_dir=$1
    label=$2
    preset=$3
    mkdir $curve_dir
    cd $curve_dir
    cmake -DLABEL=$label ..
    ../preset/$preset.sh ../
    make
    cd ..
}

# Installer les différentes courbes
install_curve "target-382" "bn382" "x64-pbc-bn382"
install_curve "target-377" "bls377" "x64-pbc-bls12-377"
install_curve "target-381" "bls381" "x64-pbc-bls12-381"
install_curve "target-330" "kss330" "x64-pbc-kss16-330"
install_curve "target-354" "kss354" "x64-pbc-kss18-354"
install_curve "target-315" "bls315" "x64-pbc-bls24-315"
install_curve "target-317" "bls317" "x64-pbc-bls24-317"

