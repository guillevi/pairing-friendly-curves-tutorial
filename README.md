# A tutorial on pairing-friendly curves for protocol designers

Authors: Anaïs Barthoulot (LIRMM) and Aurore Guillevic (Inria Rennes - IRISA)

Cryptographers designing pairing-based protocols often approach the task in a black box manner. Thanks to the work of researchers like Galbraith, Paterson, and Smart [`ePrint 2006/165`](https://eprint.iacr.org/2006/165), who have highlighted some common misconceptions, we can now assume that protocol designers are employing pairings and elliptic curves more judiciously. However, there remains a gap between designing these protocols and implementing them effectively, primarily due to the challenge of selecting an appropriate pairing-friendly elliptic curve. In this tutorial, we focus on the key considerations for implementing a pairing-based protocol. We also provide a comprehensive survey of pairing-friendly elliptic curves suitable for a 128-bit security level, detailing their properties and offering guidelines to help protocol designers select the curves that best meet their specific requirements.

## Elliptic Curves and Pairings: Simplified for Non-Experts
For protocol designers, pairings are defined as an efficiently computable, non-degenerate bilinear map $e$ from **G**<sub>1</sub>  $\times$ **G**<sub>2</sub> $\rightarrow$ **G**<sub>T</sub>, where **G**<sub>1</sub> , **G**<sub>2</sub>  and **G**<sub>T</sub> are groups of the same order $N$ (note that in this work we will only consider type 3 pairings for security and efficiency reasons already mentionned in previous works). In most cases, $N$ is considered as a prime number $p$ for efficiency reasons. To choose an appropriate pairing-friendly curve, it is essential to understand how the characteristics of the curve will impact the protocol's security and efficiency. Therefore, we need to delve a bit deeper into the details of elliptic curves while keeping the explanations accessible for readers who may not be familiar with the topic.


Currently, only known pairings suitable for cryptography are the Weil and the Tate pairings, on elliptic curves for the most efficient cases. Let $E$ be an elliptic curve, over a finite field **F**<i><sub>p</sub></i> of caracteristic $p$, where $p$ is a prime number different from 2 and 3. Therefore, $E$ can be expressed in short Weierstrass form: $y^2=x^3+ax+b$ with $a,b \in \mathbb{F}_p$. These two pairings require **G**<sub>1</sub> and **G**<sub>2</sub> to be two disjoint cyclic subgroups of the same prime order **r**. Let us define these two groups.

*Defining **G**<sub>1</sub>:* 
**G**<sub>1</sub> is defined as a subgroup of <i>E</i>(**F**<i><sub>p</sub></i>), where  $E(\mathbb{F}_p)$ denotes the set of points $(x,y)$ of $E$ whose coordinates belong to $\mathbb{F}_p$. Note that $E(\mathbb{F}_p)$ has an efficient group law. 

Depending on the curves, **G**<sub>1</sub> order can either be of the same size as the characteristic _p_ of **F**<i><sub>p</sub></i>:
#**G**<sub>1</sub> = _r_ = #<i>E</i>(**F**<i><sub>p</sub></i>) = _p_ + 1 - _t_
(it is the case for example for Barreto-Naehrig (BN) curves),
or it can be equal to a proper divisor $r \in \mathbb{N}$ of the curve order, with
$r|\#E(\mathbb{F}_p)$. The order $r$ is always coprime to $p$.
(There exist _anomalous_ curves but they are broken).
In the latter case, the curve order admits a _cofactor_ usually denoted _h_,
such that $\# E(\mathbb{F}_p) = h \cdot r$. This is the case for the `Ed25519`
(or `Curve25519`) for instance, where _h_ = 8.


Let $\mathcal{O}$ be the point at infinity, $r[P] = \underbrace{P+P \cdots + P}_{n \text{ times}}$ and let $E(\mathbb{F}_p)[r] = \left \{P \in E(\mathbb{F}_p) \mid r[P] = \mathcal{O}\right \}$ be a subgroup of order _r_ of $E(\mathbb{F}_p)[r]$. Note that if _r_ divides #<i>E</i>(**F**<i><sub>p</sub></i>) and $r^2$ does **not** divide #<i>E</i>(**F**<i><sub>p</sub></i>), then $E(\mathbb{F}_p)[r]$ is unique.
For modified Weil and Tate pairings, **G**<sub>1</sub> is set to be that subgroup.


*Defining **G**<sub>2</sub>:* Let _k_ be an integer, called the **embedding degree** of the curve. _k_ is chosen such that $\mathbb{F}_{p^k}$ is the smallest extention of $\mathbf{F}_p$ such that <i>E</i>(**F**<i><sub>p<sup>k</sup></sub></i>), that represents the group of points that belong to the curve, whose coordinates are in the field extension **F**<i><sub>p<sup>k</sup></sub></i>, captures more points _r_-torsion points that belong to **F**<i><sub>p<sup>k</sup></sub></i> \ **F**<i><sub>p</sub></i>.

**G**<sub>2</sub> is set to be a specific subgroup
of 
$E(\mathbb{F}_{p^k})[r] = \left \{P \in E(\mathbb{F}_{p^k}) \mid r[P] = \mathcal{O}\right \}$.

But there the latter contains are _r_<sup>2</sup> elements, in other words
there are _r_ - 1 ways to choose a subgroup of order _r_ over $\mathbb{F}_{p^k}$
which is not **G**<sub>1</sub>.
To fix the representation and also for efficiency reasons, the ate and optimal
ate pairings set **G**<sub>2</sub> to be the subgroup of order _r_ over
$\mathbb{F}_{p^k}$ of trace zero.
The points of **G**<sub>2</sub> map to the point at infinity under the trace map:
 $$\mathrm{Trace}(P) = P + \pi(P) + \ldots + \pi^{k-1}(P) = \mathcal{O}$$

where $\pi$ is the Frobenius map, $\pi(P) = (x_P^p, y_P^p)$ for $P(x_P, y_P)$.
For a point $P$ of order $r$ and coordinates in $\mathbb{F}_{p^k}$, the easy way
to translate it to a trace-0 generator $G_2$ of $\mathbb{G}_2$ is to set
$G_2 = [k]P - \mathrm{Trace}(P)$.

*Defining **G**<sub>T</sub>:* 
Finally, **G**<sub>T</sub> is the subgroup of order _r_ of the finite field $\mathbb{F}_{p^k}^*$.




 We now give the definition of _j_-invariant, that will be needed in the following.
>**Definition**: the _j_-invariant of an elliptic curve _E_ in short Weierstrass form 
<i>y</i><sup>2</sup> = <i>x</i><sup>3</sup> + _ax_ + _b_ is
_j_ = 1728(4<i>a</i><sup>3</sup>)/(4<i>a</i><sup>3</sup> + 27<i>b</i><sup>2</sup>).

## Designing Secure and Efficient Pairing-Based Protocols: Key Considerations
When designing a protocol, the first consideration for designers (after the desired functionnalities) is the security of the built scheme. The second thing they consider is efficency: indeed their aim is to produce secure *and* efficient protocols with advanced functionnalities.

### Security 
Pairing-based cryptography is quite prolific in terms of new security assumptions, whether decisional or computational. These new assumptions cause the inputs and challenges to vary in terms of the number or the group they belong to. The common point among them is that if the discrete logarithm problem (decisional or computational) is not hard, then these assumptions do not hold. 

> **Remark:**
> As shown by Cheon [`eprint`](https://iacr.org/archive/eurocrypt2006/40040001/40040001.pdf), designing such new assumptions must be done carefully: for some configurations, some set of inputs can help to break the discrete logarithm.

Therefore, when looking to implement a pairing-based protocol, the first thing to do is to choose the security level and select the curve parameters that guarantee the chosen level. It is important to keep in mind that the hardness of the discrete logarithm in $E(\mathbb{F}_p)$ is not the same as in $\mathbb{F}_{p^k}^*$.

- Hardness of the discrete logarithm in **G**<sub>1</sub>  depends on the group order $r$, which is chosen of size equal to $2 \cdot \lambda$, where $\lambda$ represents the required security level. Thus, for $\lambda =128$, we take $r$ of 256 bits.
- In $\mathbb{F}_{p^k}^*$, the discrete logarithm problem is subexponential, thus easier. The size of the group, which depends on $p$ and $k$, must then be bigger than **G**<sub>1</sub> 's size. However it is hard to explicit a rule to choose them as it depends on variants of the Number Field Sieve (NFS) algorithm, among other things. Currently we estime that **G**<sub>T</sub>  size must be between $3000$ and $6000$ bits to have a good security.


### Efficiency
When discussing protocol efficiency, two key factors must be considered: computational and communication costs. In some protocols, only a few elements need to be transmitted, making computational cost the primary focus. Conversely, in other protocols, communication cost becomes more significant due to the transmission of many elements. In this section, we focus on both the size of elements (affecting communication cost) and the running times (affecting computational cost).

#### Elements Sizes
To argue that a protocol is efficient, cryptographers typically analyze the number of elements used. For example, it is common to encounter statements such as "our protocol is more efficient because we reduce the number of elements in that group." However, this argument is only valid if the implemented protocol uses an elliptic curve with elements of small size in that group. Now, let's examine the size of the elements in each group.

##### The size of **G**<sub>1</sub>

The size of **G**<sub>1</sub> is given by the ratio between _r_ and _p_, denoted
rho. The _rho_-value is given by the family, one can refer to Table 8.2 p.48 in
[`ePrint 2006/372`](https://eprint.iacr.org/2006/372). More details about this parameter are beyond the scope of this tutorial, as our goal is to present elliptic curves in a simple manner. For those interested in a deeper understanding, we refer to the referenced work for more comprehensive information.

##### The size of **G**<sub>2</sub>

- we recall that _k_ is the embedding degree;
- We let $d$ be the twist degree, which allows for a factor $d$ compression so that **G**<sub>2</sub> has coefficients in **F**<i><sub>p<sup>k/d</sup></sub></i>, obtained as follows:
   - **if** _k_ = 0 mod 6 and the elliptic curve _E_ has the short Weierstrass form <i>y</i><sup>2</sup> = <i>x</i><sup>3</sup> + _b_, that is, _a_ = 0 and _j_ = 0, **then** _d_ = 6;
   - **elif** _k_ = 3 mod 6 and the elliptic curve _E_ has the short Weierstrass form <i>y</i><sup>2</sup> = <i>x</i><sup>3</sup> + _b_, that is, _a_ = 0 and _j_ = 0, **then** _d_ = 3;
   - **elif** _k_ = 0 mod 4 and the elliptic curve _E_ has the short Weierstrass form <i>y</i><sup>2</sup> = <i>x</i><sup>3</sup> + _ax_, that is, _b_ = 0 and _j_ = 1728, **then** _d_ = 4;
   - **elif** _k_ = 0 mod 2 (_k_ is even) then _d_ = 2;
   - **else** _d_ = 1.

Then **G**<sub>2</sub> is a subgroup of an elliptic curve over **F**<i><sub>p<sup>k/d</sup></sub></i>.
The point coordinates (_x_, _y_) take their values in **F**<i><sub>p<sup>k/d</sup></sub></i>.

##### The size of **G**<sub>T</sub>
The size of **G**<sub>T</sub> is given by _p_ and _k_: it is equal to _p<sup>k</sup>_, in other words its size is $k \log_2 p$ bits.

#### Running Times
Pairing is a costly operation, a fact that is widely recognized by the cryptographic community, even among those with limited knowledge of elliptic curves and pairings. Consequently, it is common to see claims of improvement in new protocols by reducing the number of pairings compared to earlier works. However, other operations within the protocol can also be resource-intensive. For example, computations in the group **G**<sub>2</sub> are expensive due to the larger size of the elements involved.

Therefore, when aiming to reduce the running time of a protocol, it is crucial to identify which operations are the most costly and performed most frequently, in order to select a curve that offers faster execution for those specific operations. Unfortunately, there is no generic method to determine the running times of operations across different curves, except by delving into technical details or conducting benchmarks. In this tutorial, we propose a benchmark to assist protocol designers in making these decisions.

> **Remark**:  It is commonly known that the BLS12-381 curve was designed to have the fastest pairing.


## Sizes and Running Times at the 128-bit security level

In the following table, the sizes are such that if possible, _p_ (and possibly
_r_) match a multiple of 64, allowing an optimal representation in 64-bit
architecture. It is always possible to choose a particular curve so that _p_ is
slightly shorter by a few bits, to allow lazy reduction with the modular
arithmetic.

| Curve | _k_ | _d_ | _p_ (bits) | _r_ (bits) | <i>p<sup>k/d</sup></i> | <i>p<sup>k/d</sup></i> (bits) | <i>p<sup>k</sup></i> (bits)|
|:------|:---:|:---:|:-------:|:--------:|:--------:|:--------:|:-----------:|
| BN    |  12 |   6 |     384 |      384 | <i>p<sup>2</sup></i> |      768 |      4608 |
| BLS12 |  12 |   6 |     384 |      256 | <i>p<sup>2</sup></i> |      768 |      4608 |
| KSS16 |  16 |   4 |     330 |      256 | <i>p<sup>4</sup></i> |     1320 |      5280 |
| KSS18 |  18 |   6 |     348 |      256 | <i>p<sup>3</sup></i> |     1044 |      6264 |
| BLS24 |  24 |   6 |     320 |      256 | <i>p<sup>4</sup></i> |     1280 |      7680 |


We now present a benchmark of running times for different 128-bit security curves. We used Relic to obtain these results, which are measured in cycles. Note that not all 128-bit security curves are implemented in Relic.

The results given in the following table are rounded up and given in **thousands of cycles**.

| Operation/Curve             | BN12-P382 | BLS12-P377 | BLS12-P381 | KSS16-P330 | KSS18-P354 | BLS24-P315 | BLS24-P317 |
|:----------------------------|----------:|-----------:|-----------:|-----------:|-----------:|-----------:|-----------:|
| Randomness in **F**<i><sub>p</sub></i>       | 3.57      | 3.56      | 3.56      | 3.50      | 3.58      | 3.63      | 3.54 |
| Randomness in **G**<sub>1</sub>      | 432     | 266     | 226    | 303     | 297     | 211     | 223
| Randomness in **G**<sub>2</sub>      | 1178      | 1036      | 628     | 2710      | 1808     | 3554      | 1772 |
| Randomness in **G**<sub>T</sub>      | 1754      | 3053     | 2134     | 6939     | 5678     | 11782     |5087 | 
| $[a]g_1$                    |  887     | 567      | 484     | 727     | 615     | 423     | 481 |
| $[a]g_2$                    |  1454    | 1375      | 862     | 4873      | 2773      | 3648      | 1830 |
| $g_T^a$                     | 1837      | 1849      | 1329     | 2894      | 3390    | 5684      | 2457 |
| $g_1 + g_1^{'}$             | 3.72      | 3.24     | 2.71     | 3.49      | 3.77      | 2.59     | 2.68 |
| $g_2 + g_2^{'}$             | 9.11    |   11.2   | 7.44     | 27.2     | 19.5      | 41.2      |19.6 |
| $g_T \cdot g_T^{'}$         | 11.3    | 15.5     | 11.3     | 20.1      | 25.5     | 58.4      | 26.1 |
| $-g_1$                      | 0.12     | 0.13      | 0.12      | 0.13     | 0.12     | 0.14     | 0.11 |
| $-g_2$                      | 0.18      | 0.20      |0.18     | 0.27      | 0.23      | 0.44      | 0.27 |
| $g_T^{-1}$                  | 0.17     | 0.17      |0.18      | 0.23     | 0.26      | 0.44      | 0.46 |
| $e(g_1, g_2)$               |  4165    | 5228    | 3705     | 9259      | 7994      | 16600      |7043 |
| $\mathcal{H} \rightarrow$ **G**<sub>1</sub> | 463      | 1353      | 364      | 755      | 764      | 582      | 459 |
| $\mathcal{H} \rightarrow$ **G**<sub>2</sub> | 1658     | 3633     | 1204      | 6913      | 3428      | 5875      |3121 |
| **Sum**                     |  13856.17  | 18394.00  | 10961.49  | 35427.92  | 26799.96  | 48465.84  | 22525.76  |

## Guidelines to Choose an Appropriate Curve
We now present our guidelines to choose wisely a pairing-friendly elliptic curve to implement a pairing-based protocol. Our methodology is presented to choose a pairing-friendly elliptic curve with _128_ bits of security, but it can be applied to any security level as long as a state of the art regarding sizes and running times is available.

To correctly choose the curve you should use, answer the different following questions. 
1. Do you have to do 
    1. many hashing into
        1. group **G**<sub>1</sub>?
        2. group **G**<sub>2</sub>?
    2. many scalar multiplications on
        1. group **G**<sub>1</sub>?
        2. group **G**<sub>2</sub>?
    3. many exponentions in **G**<sub>T</sub>?
    4. many additions in 
        1. group **G**<sub>1</sub>?
        2. group **G**<sub>2</sub>?
    5. many multiplications in **G**<sub>T</sub>?
    6. many negations in
        1. group **G**<sub>1</sub>?
        2. group **G**<sub>2</sub>?
    7. many inversions in **G**<sub>T</sub>?
    8. many pairings?
2. Do you have to send a lot of elements from 
    1. group **G**<sub>1</sub>?
    2. group **G**<sub>2</sub>?
    3. group **G**<sub>T</sub>?

If you answer ''yes'' for one sub-question of question 1, then you need to choose a curve with that specific operation being fast. If you answer ''yes'' for one sub-question of question 2, then you need to choose a curve with that specific group elements with small size.

> Let us take an example: imagine that you want to implement a protocol in which one entity has limited memory and computational power. This entity has to store several elements of one group and perform some computations on it. Later, she sends her elements to another unconstrained entity. In that case, it is more relevant to choose a curve with a group that has small element sizes and efficient operations. The group with smaller element sizes is G<sub>1</sub>, especially with curves BLS24-P315 and BLS24-P317. Looking at these curves, the former has more efficient operations over G<sub>1</sub>, except for hashing (and negation but the difference is really small). Therefore, if no hashing (or very little hashing) is required, BLS24-P315 is more appropriate. If a lot of hashing is required, then choosing BLS24-P317 is a better choice, or even BLS12-P381 as, at the cost of bigger element sizes, hashing into G<sub>1</sub> is more efficient than with BLS24-P317. But, if a lot of randomness should be generated in G<sub>1</sub>, choosing BLS24-P317 seems more appropriate.
>
> If the protocol uses a lot of pairings, for example for decryption, and we want the latter to be done quickly, then we need to choose the curve BLS12-P381.

Depending on your protocol's specific requirements, you determine which of the above questions are more important. Then, by examining the benchmarking tables, you can identify the curve that offers better efficiency for your key operations. This will guide you in selecting the most suitable curve to implement your protocol.

Let us try to formalize and automate the choice. First, we present a table with normalized score of each curve, regarding elements' sizes and operations' running times.

| Operation/Curve   |   BN12-P382 |   BLS12-P377 |   BLS12-P381 |   KSS16-P330 |   KSS18-P354 |   BLS24-P315 |   BLS24-P317 |
|:------------------|------------:|-------------:|-------------:|-------------:|-------------:|-------------:|-------------:|
| Randomness in Fp  |     1.02    |      1.02    |      1.02    |      1       |      1.02    |      1.04    |      1.01    |
| Randomness in G1  |     2.05    |      1.26    |      1.07    |      1.44    |      1.41    |      1       |      1.06    |
| Randomness in G2  |     1.88    |      1.65    |      1       |      4.32    |      2.88    |      5.66    |      2.82    |
| Randomness in GT  |     1       |      1.74    |      1.22    |      3.96    |      3.24    |      6.72    |      2.90    |
| [a]g1             |     2.10    |      1.34    |      1.14    |      1.72    |      1.45    |      1       |      1.14    |
| [a]g2             |     1.69    |      1.60    |      1       |      5.65    |      3.22    |      4.23    |      2.12    |
| g_T^a             |     1.38    |      1.39    |      1       |      2.18    |      2.55    |      4.28    |      1.85    |
| g_1 + g_1'        |     1.44    |      1.25    |      1.05    |      1.35    |      1.46    |      1       |      1.03    |
| g_2 + g_2'        |     1.22    |      1.51    |      1       |      3.66    |      2.62    |      5.54    |      2.63    |
| g_T * g_T'        |     1       |      1.37    |      1       |      1.78    |      2.26    |      5.17    |      2.31    |
| -g_1              |     1.09    |      1.18    |      1.09    |      1.18    |      1.09    |      1.27    |      1       |
| -g_2              |     1       |      1.11    |      1       |      1.50    |      1.28    |      2.44    |      1.50    |
| g_T^{-1}          |     1       |      1       |      1.06    |      1.35    |      1.53    |      2.59    |      2.71    |
| e(g_1, g_2)       |     1.12    |      1.41    |      1       |      2.50    |      2.16    |      4.48    |      1.90    |
| H -> G1           |     1.27    |      3.72    |      1       |      2.07    |      2.10    |      1.60    |      1.26    |
| H -> G2           |     1.38    |      3.02    |      1       |      5.74    |      2.85    |      4.88    |      2.59    |
| **Total Sum** | 21.63 | 25.56 | 16.65 | 41.39 | 33.10 | 52.89 | 29.84 |


The above sum highlight the fact that in general BLS12-P381 is the best choice of curve. 


## References

- Paulo S. L. M. Barreto, Craig Costello, Rafael Misoczki, Michael Naehrig, Geovandro C. C. F. Pereira, and Gustavo Zanon.
  Subgroup security in pairing-based cryptography.
  [`ePrint 2015/247`](https://eprint.iacr.org/2015/247)
- David Freeman, Michael Scott, and Edlyn Teske.
  A Taxonomy of Pairing-friendly Elliptic Curves.
  [`ePrint 2006/372`](https://eprint.iacr.org/2006/372)
- Steven D. Galbraith, Kenneth G. Paterson, and Nigel P. Smart.
  Pairings for Cryptographers.
  Discrete Applied Mathematics 156(16): 3113-3121 (2008)
  [`ePrint 2006/165`](https://eprint.iacr.org/2006/165)
- Aurore Guillevic.
  A short-list of pairing-friendly curves resistant to Special TNFS at the 128-bit security level.
  [`ePrint 2019/1371`](https://eprint.iacr.org/2019/1371)
- Aurore Guillevic.
  Pairing-friendly curves blog post.
  [blog](https://members.loria.fr/AGuillevic/pairing-friendly-curves/)
- Jung Hee Cheon.
  Security Analysis of the Strong Diffie-Hellman Problem.
  [`eprint Eurocrypt 2006 archive`](https://iacr.org/archive/eurocrypt2006/40040001/40040001.pdf)

## Appendix
### Sizes Benchmark

We now present sizes obtained from a benchmark done with Relic. Sizes are in bytes.
Curves |Sizes of **G**<sub>1</sub> elements|Sizes of **G**<sub>2</sub> elements|Sizes of **G**<sub>T</sub> elements|
|------|-----|------|-----|
BN12-P382| 49 | 97 | 384 |
BLS12-P377 | 49 | 97 | 576 |
BLS12-P381| 49 | 97 | 576 |
KSS16-P330 | 43 | 337 | 336 |
BLS24-P315 |41 | 321 | 640 |
BLS24-P317 | 41 | 321 | 640 |

>**Remark:** Something surprising happened: sizes for elements in G<sub>T</sub> differ in the benchmark for the same curve. The first time the function is called, it returns a bigger size than the subsequent times. For example, for BN12-P382, it returns 576 on the first call, then 384 each subsequent time. The same happens for other curves.
